#include "get_next_line.h"

char	*ft_lsttostr(t_list *lst)
{
	int		i;
	char	*res;
	int		len;

	res = malloc((ft_lstsize(lst) + 1) * sizeof(char));
	if (res == NULL)
		return (NULL);
	while (lst != NULL)
	{
		res[i] = (char)lst->content;
		lst = lst->next;
		i++;
	}
	res[i] = '\0';
	ft_lstfree(&lst);
	return (res);
}

t_list  *ft_memtolst(char *mem, int size)
{
	int 	i;
	t_list  *lst;
	t_list	*new;

	i = 0;
	lst = NULL;
	while (i < size)
	{
		new = ft_lstnew(mem[i]);
		if (new == NULL)
		{
			ft_lstfree(&lst);
			return (NULL);
		}
		ft_lstadd_back(&lst, new);
		i++;
	}
	return (lst);
}

int	get_next_line(int fd, char **line)
{
	static char		buf[BUFFER_SIZE] = "";
	unsigned int	rread;
	t_list			*lst;

    lst = NULL;
	if (buf[0] != '\0')
	{
		lst = ft_memtolst(buf, ft_strslen(buf, BUFFER_SIZE));
		ft_bzero(buf, BUFFER_SIZE);
	}
	while (1)
	{
		rread = read(fd, buf, BUFFER_SIZE);
		if (rread == 0)
		{
			*line = ft_lsttostr(lst);
			ft_bzero(buf, BUFFER_SIZE);
			return (0);
		}
		else if (rread < BUFFER_SIZE)
		{
			ft_lstadd_back(&lst, ft_memtolst(buf, rread));
			*line = ft_lsttostr(lst);
			ft_bzero(buf, BUFFER_SIZE);
			return (0);
		}
		else if (ft_memchr(buf, '\n', BUFFER_SIZE) != NULL)
		{
			ft_lstadd_back(&lst, ft_memtolst(buf, BUFFER_SIZE - (ft_memchr(buf, '\n', BUFFER_SIZE) - buf)));
			*line = ft_lsttostr(lst);
			ft_memmove(ft_memchr(buf, '\n', BUFFER_SIZE) + 1, buf, BUFFER_SIZE - (ft_memchr(buf, '\n', BUFFER_SIZE) - buf) - 1);
			ft_bzero(buf + BUFFER_SIZE - ((ft_memchr(buf, '\n', BUFFER_SIZE)) - buf), ((ft_memchr(buf, '\n', BUFFER_SIZE)) - buf) - 1);
			return (1);
		}
		else if (ft_memchr(buf, '\n', BUFFER_SIZE) == NULL)
		{
			ft_lstadd_back(&lst, ft_memtolst(buf, BUFFER_SIZE));
			ft_bzero(buf, BUFFER_SIZE);
		}
	}
}
