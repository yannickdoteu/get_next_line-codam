void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t			i;
	unsigned char	*ptrs;

	ptrs = (unsigned char *)s;
	i = 0;
	while (i < n)
	{
		if (*(ptrs + i) == (unsigned char)c)
			return ((void *)(ptrs + i));
		i++;
	}
	return (NULL);
}

int	ft_lstsize(t_list *lst)
{
	int		i;

	i = 0;
	while (lst != NULL)
	{
		lst = lst->next;
		i++;
	}
	return (i);
}

void	ft_lstfree(t_list *lst)
{
	t_list	*next;

	if (*lst == NULL)
		return ;
	while (*lst)
	{
		next = (*lst)->next;
		free(*lst);
		*lst = next;
	}
	return (NULL);
}

t_list	*ft_lstnew(void *content)
{
	t_list	*new;

	new = ft_calloc(1, sizeof(t_list));
	if (new == NULL)
		return (NULL);
	new->content = content;
	new->next = NULL;
	return (new);
}

void	ft_lstadd_back(t_list **lst, t_list *new)
{
	if (*lst == NULL)
		*lst = new;
	else
		ft_lstlast(*lst)->next = new;
}

size_t	ft_strslen(const char *s, size_t size)
{
	int	i;

	while (s[i] && i < size)
		i++;
	return (i);
}

void	ft_bzero(void *s, size_t n)
{
	size_t	i;
	char	*ptr;

	ptr = s;
	i = 0;
	if (n > 0)
	{
		while (i < n)
		{
			ptr[i] = 0;
			i++;
		}
	}
}

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	size_t		i;

	if (dest < src)
	{
		i = 0;
		while (i < n)
		{
			*((unsigned char *)dest + i) = *((unsigned char *)src + i);
			i++;
		}
	}
	if (dest > src)
	{
		i = n - 1;
		while (i < n)
		{
			*((unsigned char *)dest + i) = *((unsigned char *)src + i);
			i--;
		}
	}
	return (dest);
}
